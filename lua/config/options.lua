local options = {
	termguicolors = true,
	mouse = "a",

	tabstop = 4,
	softtabstop = 4,
	shiftwidth = 4,
	expandtab = false,
	smarttab = true,

	smartindent = true,
	wrap = false,

	swapfile = false,
	backup = false,
	undofile = true,

	hlsearch = true,
	incsearch = true,

	ignorecase = true,
	smartcase = true,

	nu = true,
	relativenumber = true,

	scrolloff = 7,
	signcolumn = "yes",

	updatetime = 49,

	completeopt = "menuone,noselect",
	clipboard = "unnamedplus",
	inccommand = "nosplit",
	splitright = true,
	splitbelow = true,
}

for k, v in pairs(options) do
	vim.opt[k] = v
end

vim.wo.signcolumn = "yes"

vim.cmd("highlight cursorlinenr cterm=bold")
